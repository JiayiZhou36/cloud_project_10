# Cloud Project 10
By Jiayi Zhou
[![pipeline status](https://gitlab.com/JiayiZhou36/cloud_project_10/badges/main/pipeline.svg)](https://gitlab.com/JiayiZhou36/cloud_project_10/-/commits/main)

## Purpose of Project
This project creates a serverless AWS Lambda function using the Rust programming language and Cargo Lambda to deploy a local llm inference endpoint.

## Requirements
* Dockerize Hugging Face Rust transformer
* Deploy container to AWS Lambda
* Implement query endpoint

## Docker Image
![Screenshot_2024-04-13_at_5.14.11_PM](/uploads/b4148c2cf09d49ac8299e0644847276c/Screenshot_2024-04-13_at_5.14.11_PM.png)

## AWS Lambda
[Function URL](https://msyrhbdivzfr7lyv5xv2fyowga0wvitc.lambda-url.us-east-1.on.aws/)
![Screenshot_2024-04-13_at_6.45.31_PM](/uploads/8a8ab6c616a163d061421531e0c4e3a6/Screenshot_2024-04-13_at_6.45.31_PM.png)

## Query Endpoint
![Screenshot_2024-04-13_at_6.07.29_PM](/uploads/71c17356d27ef6531b24e8d66b754b3a/Screenshot_2024-04-13_at_6.07.29_PM.png)